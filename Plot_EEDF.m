% Plot_EEDF

figure (1); clf;
figure1 = gcf;
set(figure1,'Color',[1 1 1]); 
axes1 = axes('Parent',figure1);
hold(axes1,'on');
set(axes1,'FontName','Times New Roman','FontSize',24,'LineWidth',2);
hold on
plot(ue/qe,abs(f_0),'Color','r','LineWidth',2,'LineStyle','-')
ylabel('EEDF (eV^{-3/2})')
grid on
box on
set(axes1,'MinorGridColor','w','GridLineStyle','--')
set(axes1,'YScale','log')
xlabel('Electron Energy (eV)')
pause(0.1)